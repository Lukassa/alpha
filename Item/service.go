package Item

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/jinzhu/gorm"
	"net/http"
	"strconv"
)

type Service interface {
	CreateCharacter(ctx *gin.Context)
}

func GetAllItems(context *gin.Context) {
	r := NewItemRepository(context.MustGet("db").(*gorm.DB))
	item, err := r.FindAll()
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"data": item})
	}
}

func GetItemsById(context *gin.Context) {
	id, _ := strconv.ParseUint(context.Param("id"), 10, 64)
	r := NewItemRepository(context.MustGet("db").(*gorm.DB))
	item, err := r.FindById(uint(id))
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"data": item})
	}
}

func CreateItems(context *gin.Context) {
	var data Item
	if err := context.BindWith(&data, binding.JSON); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	r := NewItemRepository(context.MustGet("db").(*gorm.DB))
	item, err := r.Save(data)
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"data": item})
	}
}

func PatchItems(context *gin.Context) {
	r := NewItemRepository(context.MustGet("db").(*gorm.DB))
	var data Item
	if err := context.BindJSON(&data); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := r.Update(data)
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"Success": "Updated successfully"})
	}
}

func DeleteItems(context *gin.Context) {
	r := NewItemRepository(context.MustGet("db").(*gorm.DB))
	var data Item
	if err := context.BindJSON(&data); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := r.Delete(data)
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"Success": "Item was successful deleted."})
	}
}
