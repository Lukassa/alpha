package Item

import "github.com/gin-gonic/gin"

func Routes(router *gin.RouterGroup) {
	items := router.Group("/items/")
	items.GET("", GetAllItems)
	items.GET(":id", GetItemsById)
	items.POST("", CreateItems)
	items.PATCH(":id", PatchItems)
	items.DELETE(":id", DeleteItems)
}
