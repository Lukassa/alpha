package Item

import (
	"github.com/jinzhu/gorm"
)

type Repository interface {
	FindAll() ([]Item, error)
	FindById(uint) (Item, error)
	Save(Item) (Item, error)
	Update(Item) error
	Delete(Item) error
	Count() (uint, error)
}

type itemRepository struct {
	*gorm.DB
}

func NewItemRepository(db *gorm.DB) Repository {
	return &itemRepository{DB: db}
}

func (r itemRepository) FindAll() ([]Item, error) {
	var entities []Item
	err := r.DB.Find(&entities).Error
	return entities, err
}

func (r itemRepository) FindById(id uint) (Item, error) {
	var entity Item
	err := r.DB.First(&entity, id).Error
	return entity, err
}

func (r itemRepository) Save(entity Item) (Item, error) {
	err := r.DB.Create(&entity).Error
	return entity, err
}

func (r itemRepository) Update(entity Item) error {
	return r.DB.UpdateColumns(&entity).Error
}

func (r itemRepository) Delete(entity Item) error {
	return r.DB.Delete(&entity).Error
}

func (r itemRepository) Count() (uint, error) {
	var count uint
	err := r.DB.Model(&Item{}).Count(&count).Error
	return count, err
}
