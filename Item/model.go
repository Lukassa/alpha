package Item

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/Lukassa/alpha/ItemType"
)

type Item struct {
	gorm.Model
	Name      string
	Unique    bool
	Stackable uint
	Type      ItemType.ItemType
	TypeId    uint
}
