module gitlab.com/Lukassa/alpha

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.2
	github.com/go-playground/assert/v2 v2.0.1
	github.com/jinzhu/gorm v1.9.12
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20200414173820-0848c9571904
)
