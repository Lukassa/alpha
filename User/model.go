package User

import "github.com/jinzhu/gorm"

type User struct {
	gorm.Model
	Email     string `gorm:"type:varchar(100);unique_index"`
	FirstName string `gorm:"size:80"`
	LastName  string `gorm:"size:80"`
	Password  string `json:"password"`
}
