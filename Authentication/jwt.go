package Authentication

import (
	"github.com/dgrijalva/jwt-go"
	. "gitlab.com/Lukassa/alpha/User"
	"golang.org/x/crypto/bcrypt"
	"time"
)

func EncryptPassword(userPw string) (string, error) {
	pass, err := bcrypt.GenerateFromPassword([]byte(userPw), bcrypt.DefaultCost)
	return string(pass), err
}

func ValidatePassword(givenPw string, userPw string) error {
	err := bcrypt.CompareHashAndPassword([]byte(givenPw), []byte(userPw))
	if err != nil {
		return err
	}
	return nil
}

func CreateJwtToken(user *User) (string, error) {
	tk := Token{
		UserId:    user.ID,
		Email:     user.Email,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * 100000).Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), *tk.StandardClaims)

	tokenString, err := token.SignedString([]byte("secret"))
	return tokenString, err
}
