package Authentication

import (
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/bcrypt"
	"testing"
)

/*
func TestCreateJwtToken(t *testing.T) {
	type args struct {
		user *User
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CreateJwtToken(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateJwtToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CreateJwtToken() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCreateUser(t *testing.T) {
	type args struct {
		ctx *gin.Context
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
		})
	}
}

*/

func TestEncryptPassword(t *testing.T) {
	type args struct {
		userPw string
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			"Lukas",
			args{userPw: "test"},
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := EncryptPassword(tt.args.userPw)
			assert.Equal(t, err, tt.wantErr)
			assert.NotEqual(t, got, nil)
		})
	}
}

func TestPwValidation(t *testing.T) {
	type args struct {
		t             *testing.T
		givenPassword string
		password      string
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			"positive",
			args{t: t, password: "test", givenPassword: "$2a$10$YEt3HCIMwq8Xq1AfMD7iRuScMdlplFMvvA9lta1HP7was1TWRUS3CK"},
			nil,
		},
		{
			"negative",
			args{t: t, password: "tes", givenPassword: "$2a$10$YEt3HCIMwq8Xq1AfMD7iRuScMdlplFMvvA9lta1HP7was1TWRUS3CK"},
			bcrypt.ErrMismatchedHashAndPassword,
		},
		{
			"empty String",
			args{t: t, password: "", givenPassword: "$2a$10$YEt3HCIMwq8Xq1AfMD7iRuScMdlplFMvvA9lta1HP7was1TWRUS3CK"},
			bcrypt.ErrMismatchedHashAndPassword,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := ValidatePassword(tt.args.givenPassword, tt.args.password)
			assert.Equal(t, result, tt.want)
		})
	}
}

/**
func TestFindUser(t *testing.T) {
	type args struct {
		ctx   *gin.Context
		email *string
	}
	tests := []struct {
		name    string
		args    args
		want    *User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := FindUser(tt.args.ctx, tt.args.email)
			if (err != nil) != tt.wantErr {
				t.Errorf("FindUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindUser() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestJwtVerify(t *testing.T) {
	tests := []struct {
		name string
		want gin.HandlerFunc
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := JwtVerify(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("JwtVerify() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLogin(t *testing.T) {
	type args struct {
		ctx *gin.Context
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
		})
	}
}

func TestRoutes(t *testing.T) {
	type args struct {
		router *gin.RouterGroup
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
		})
	}
}

func TestSaveUser(t *testing.T) {
	type args struct {
		ctx  *gin.Context
		user *User
	}
	tests := []struct {
		name    string
		args    args
		want    *User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := SaveUser(tt.args.ctx, tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("SaveUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SaveUser() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestValidatePassword(t *testing.T) {
	type args struct {
		givenPw string
		userPw  string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidatePassword(tt.args.givenPw, tt.args.userPw); (err != nil) != tt.wantErr {
				t.Errorf("ValidatePassword() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
*/
