package Authentication

import (
	"github.com/dgrijalva/jwt-go"
)

type Token struct {
	UserId         uint
	Email          string
	FirstName      string
	LastName       string
	StandardClaims *jwt.StandardClaims
}
