package Authentication

import "github.com/gin-gonic/gin"

func Routes(router *gin.RouterGroup) {
	router.POST("/login/", Login)
	router.POST("/register/", CreateUser)
	router.GET("/logout/")
}
