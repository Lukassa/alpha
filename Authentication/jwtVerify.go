package Authentication

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
)

func JwtVerify() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenHeader := c.GetHeader("x-access-token")
		if tokenHeader == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error:": "No x-access-token header with a valid JWT-Token attached."})
			c.Abort()
			return
		}

		tk := &Token{
			StandardClaims: &jwt.StandardClaims{
				ExpiresAt: 0,
			},
		}
		_, err := jwt.ParseWithClaims(tokenHeader, tk.StandardClaims, func(token *jwt.Token) (interface{}, error) {
			return []byte("secret"), nil
		})

		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error:": err.Error()})
			c.Abort()
		}
		c.Next()
	}

}
