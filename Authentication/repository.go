package Authentication

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	. "gitlab.com/Lukassa/alpha/User"
)

func SaveUser(ctx *gin.Context, user *User) (*User, error) {
	db := ctx.MustGet("db").(*gorm.DB)
	return user, db.Create(&user).Error
}

func FindUser(ctx *gin.Context, email *string) (*User, error) {
	db := ctx.MustGet("db").(*gorm.DB)
	var currentUser User
	if err := db.First(&currentUser, "Email = ?", email).Error; err != nil {
		return &currentUser, err
	}
	return &currentUser, nil
}
