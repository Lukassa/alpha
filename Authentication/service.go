package Authentication

import (
	. "gitlab.com/Lukassa/alpha/User"
	"net/http"
)
import "github.com/gin-gonic/gin"

func Login(ctx *gin.Context) {
	var user User
	if err := ctx.BindJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	if currentUser, err := FindUser(ctx, &user.Email); err != nil {
		ctx.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		if err := ValidatePassword(currentUser.Password, user.Password); err != nil {
			ctx.JSON(http.StatusUnauthorized, gin.H{"error:": err.Error()})
		} else {
			token, err := CreateJwtToken(currentUser)
			if err != nil {
				ctx.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
			}
			ctx.JSON(http.StatusOK, gin.H{"token": token, "data": currentUser})
		}
	}
}

func CreateUser(ctx *gin.Context) {
	var user User
	if err := ctx.BindJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	if pass, err := EncryptPassword(user.Password); err != nil {
		ctx.JSON(http.StatusConflict, gin.H{"error": err.Error()})
	} else {
		user.Password = pass
	}
	if newUser, err := SaveUser(ctx, &user); err != nil {
		ctx.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"data": newUser})
	}
}
