package Character

type CreateCharacterInput struct {
	GuildID          uint   `json:"guildId" binding:"required"`
	LearningPoints   uint8  `json:"learningPoints"`
	ExperiencePoints uint8  `json:"experiencePoints"`
	Life             uint   `json:"life" binding:"required"`
	Mana             uint   `json:"mana" binding:"required"`
	Strength         uint   `json:"strength" binding:"required"`
	Dexterity        uint   `json:"dexterity" binding:"required"`
	Scene            string `json:"scene" binding:"required"`
}
