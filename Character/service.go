package Character

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/jinzhu/gorm"
	"net/http"
	"strconv"
)

type Service interface {
	CreateCharacter(ctx *gin.Context)
}

func GetAllCharacters(context *gin.Context) {
	r := NewCharacterRepository(context.MustGet("db").(*gorm.DB))
	characters, err := r.FindAll()
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"data": characters})
	}
}

func GetCharacterById(context *gin.Context) {
	id, _ := strconv.ParseUint(context.Param("id"), 10, 64)
	r := NewCharacterRepository(context.MustGet("db").(*gorm.DB))
	character, err := r.FindById(uint(id))
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"data": character})
	}
}

func CreateCharacter(context *gin.Context) {
	var validator CreateCharacterInput
	if err := context.ShouldBindBodyWith(&validator, binding.JSON); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var data Character
	if err := context.BindWith(&data, binding.JSON); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	r := NewCharacterRepository(context.MustGet("db").(*gorm.DB))
	character, err := r.Save(data)
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"data": character})
	}
}

func PatchCharacter(context *gin.Context) {
	r := NewCharacterRepository(context.MustGet("db").(*gorm.DB))
	var data Character
	if err := context.BindJSON(&data); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := r.Update(data)
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"Success": "Updated successfully"})
	}
}

func DeleteCharacter(context *gin.Context) {
	r := NewCharacterRepository(context.MustGet("db").(*gorm.DB))
	var data Character
	if err := context.BindJSON(&data); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := r.Delete(data)
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"Success": "Character was successful deleted."})
	}
}
