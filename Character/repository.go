package Character

import (
	"github.com/jinzhu/gorm"
)

type Repository interface {
	FindAll() ([]Character, error)
	FindById(uint) (Character, error)
	Save(Character) (Character, error)
	Update(Character) error
	Delete(Character) error
	Count() (uint, error)
}

type characterRepository struct {
	*gorm.DB
}

func NewCharacterRepository(db *gorm.DB) Repository {
	return &characterRepository{DB: db}
}

func (r characterRepository) FindAll() ([]Character, error) {
	var entities []Character
	err := r.DB.Find(&entities).Error
	return entities, err
}

func (r characterRepository) FindById(id uint) (Character, error) {
	var entity Character
	err := r.DB.First(&entity, id).Error
	return entity, err
}

func (r characterRepository) Save(entity Character) (Character, error) {
	err := r.DB.Create(&entity).Error
	return entity, err
}

func (r characterRepository) Update(entity Character) error {
	return r.DB.UpdateColumns(&entity).Error
}

func (r characterRepository) Delete(entity Character) error {
	return r.DB.Delete(&entity).Error
}

func (r characterRepository) Count() (uint, error) {
	var count uint
	err := r.DB.Model(&Character{}).Count(&count).Error
	return count, err
}
