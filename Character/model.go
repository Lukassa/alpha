package Character

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/Lukassa/alpha/Guild"
)

type Character struct {
	gorm.Model
	Guild            Guild.Guild
	GuildID          uint64
	LearningPoints   uint64
	ExperiencePoints uint64
	Life             uint64
	Mana             uint64
	Strength         uint64
	Dexterity        uint64
	Scene            string `gorm:"size:80"`
}
