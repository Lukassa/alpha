package Character

import (
	"github.com/gin-gonic/gin"
)

func Routes(router *gin.RouterGroup) {
	character := router.Group("/characters/")
	character.GET("", GetAllCharacters)
	character.GET(":id", GetCharacterById)
	character.POST("", CreateCharacter)
	character.PATCH(":id", PatchCharacter)
	character.DELETE(":id", DeleteCharacter)
}
