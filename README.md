# Introduction

This is a API for a role play game developed in unity3d.

# Reference links


# Getting started

First clone this repo and install all dependencies

```diff
go get
```

## What's contained in this project

Nothing yet

## Dependencies

GORM: https://github.com/jinzhu/gorm

## Setup local mysql db with phpmyadmin frontend in docker
```shell
docker network create alpha

docker run -d \
    --name alpha-mysql \
    --network alpha \
    -e MYSQL_ROOT_PASSWORD="yourOwnPW" \
    -p 3306:3306 \
    mysql:8.0.12
    
docker run -d \
    --name alpha-phpmyadmin \
    --network alpha \
    -e PMA_HOST=alpha-mysql \
    -p 8080:80 \
    phpmyadmin/phpmyadmin:
```

Check if containers where created with 
```shell
docker ps -a
```
Start the containers
```shell
docker start alpha-mysql alpha-phpmyadmin
```
Stop the containers
```shell
docker stop alpha-mysql alpha-phpmyadmin
```

## Run Service

```shell
go run main.go
```

