package main

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/Lukassa/alpha/Item"
	"gitlab.com/Lukassa/alpha/ItemType"
	"log"

	"gitlab.com/Lukassa/alpha/Authentication"
	"gitlab.com/Lukassa/alpha/Character"
	"gitlab.com/Lukassa/alpha/Guild"
	"gitlab.com/Lukassa/alpha/User"
)

func start(database *gorm.DB) error {
	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Set("db", database)
		context.Next()
	})

	basic := router.Group("/v1/")

	Authentication.Routes(basic)

	basic.Use(Authentication.JwtVerify())
	Character.Routes(basic)
	Guild.Routes(basic)
	ItemType.Routes(basic)
	Item.Routes(basic)

	if err := router.Run(":8888"); err != nil {
		return nil
	}
	return nil
}

func main() {
	database := initializeDatabase()

	if err := start(database); err != nil {
		log.Fatalf("Server error: %v\n", err)
	}
}

func initializeDatabase() *gorm.DB {
	db, err := gorm.Open("mysql", "Lukas:alpha@tcp(localhost:3306)/alpha?parseTime=true")
	if err != nil {

		panic("failed to connect database")
	}

	//db.LogMode(true)
	db.AutoMigrate(
		&Character.Character{},
		&Guild.Guild{},
		&User.User{},
		&ItemType.ItemType{},
		&Item.Item{})

	return db
}
