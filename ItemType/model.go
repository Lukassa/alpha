package ItemType

import "github.com/jinzhu/gorm"

type ItemType struct {
	gorm.Model
	Name string
}
