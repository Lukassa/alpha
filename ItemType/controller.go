package ItemType

import "github.com/gin-gonic/gin"

func Routes(router *gin.RouterGroup) {
	itemTypes := router.Group("/itemTypes/")
	itemTypes.GET("", GetAllItemTypes)
	itemTypes.GET(":id", GetItemTypesById)
	itemTypes.POST("", CreateItemTypes)
	itemTypes.PATCH(":id", PatchItemTypes)
	itemTypes.DELETE(":id", DeleteItemTypes)
}
