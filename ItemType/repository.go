package ItemType

import (
	"github.com/jinzhu/gorm"
)

type Repository interface {
	FindAll() ([]ItemType, error)
	FindById(uint) (ItemType, error)
	Save(ItemType) (ItemType, error)
	Update(ItemType) error
	Delete(ItemType) error
	Count() (uint, error)
}

type itemTypeRepository struct {
	*gorm.DB
}

func NewItemTypeRepository(db *gorm.DB) Repository {
	return &itemTypeRepository{DB: db}
}

func (r itemTypeRepository) FindAll() ([]ItemType, error) {
	var entities []ItemType
	err := r.DB.Find(&entities).Error
	return entities, err
}

func (r itemTypeRepository) FindById(id uint) (ItemType, error) {
	var entity ItemType
	err := r.DB.First(&entity, id).Error
	return entity, err
}

func (r itemTypeRepository) Save(entity ItemType) (ItemType, error) {
	err := r.DB.Create(&entity).Error
	return entity, err
}

func (r itemTypeRepository) Update(entity ItemType) error {
	return r.DB.UpdateColumns(&entity).Error
}

func (r itemTypeRepository) Delete(entity ItemType) error {
	return r.DB.Delete(&entity).Error
}

func (r itemTypeRepository) Count() (uint, error) {
	var count uint
	err := r.DB.Model(&ItemType{}).Count(&count).Error
	return count, err
}
