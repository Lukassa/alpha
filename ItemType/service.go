package ItemType

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/jinzhu/gorm"
	"net/http"
	"strconv"
)

type Service interface {
	CreateItemTypes(ctx *gin.Context)
}

func GetAllItemTypes(context *gin.Context) {
	r := NewItemTypeRepository(context.MustGet("db").(*gorm.DB))
	itemTypes, err := r.FindAll()
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"data": itemTypes})
	}
}

func GetItemTypesById(context *gin.Context) {
	id, _ := strconv.ParseUint(context.Param("id"), 10, 64)
	r := NewItemTypeRepository(context.MustGet("db").(*gorm.DB))
	itemTypes, err := r.FindById(uint(id))
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"data": itemTypes})
	}
}

func CreateItemTypes(context *gin.Context) {
	var data ItemType
	if err := context.BindWith(&data, binding.JSON); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	r := NewItemTypeRepository(context.MustGet("db").(*gorm.DB))
	itemTypes, err := r.Save(data)
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"data": itemTypes})
	}
}

func PatchItemTypes(context *gin.Context) {
	r := NewItemTypeRepository(context.MustGet("db").(*gorm.DB))
	var data ItemType
	if err := context.BindJSON(&data); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := r.Update(data)
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"Success": "Updated successfully"})
	}
}

func DeleteItemTypes(context *gin.Context) {
	r := NewItemTypeRepository(context.MustGet("db").(*gorm.DB))
	var data ItemType
	if err := context.BindJSON(&data); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := r.Delete(data)
	if err != nil {
		context.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{"Success": "ItemTypes was successful deleted."})
	}
}
