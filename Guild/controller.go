package Guild

import "github.com/gin-gonic/gin"

func Routes(router *gin.RouterGroup) {
	guilds := router.Group("/guilds/")
	guilds.GET("", GetAllGuilds)
	guilds.GET(":id", GetGuildById)
}
