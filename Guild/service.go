package Guild

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"strconv"
)

func GetAllGuilds(ctx *gin.Context) {
	r := NewGuildRepository(ctx.MustGet("db").(*gorm.DB))
	guilds, err := r.FindAll()
	if err != nil {
		ctx.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"data": guilds})
	}
}

func GetGuildById(ctx *gin.Context) {
	id, _ := strconv.ParseUint(ctx.Param("id"), 10, 64)
	r := NewGuildRepository(ctx.MustGet("db").(*gorm.DB))
	guild, err := r.FindById(uint(id))
	if err != nil {
		ctx.JSON(http.StatusConflict, gin.H{"error:": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"data": guild})
	}
}
