package Guild

import (
	"github.com/jinzhu/gorm"
)

type Repository interface {
	FindAll() ([]Guild, error)
	FindById(uint) (Guild, error)
	Save(Guild) (Guild, error)
	Update(Guild) error
	Delete(Guild) error
	Count() (uint, error)
}

type guildRepository struct {
	*gorm.DB
}

func NewGuildRepository(db *gorm.DB) Repository {
	return &guildRepository{DB: db}
}

func (r guildRepository) FindAll() ([]Guild, error) {
	var entities []Guild
	err := r.DB.Find(&entities).Error
	return entities, err
}

func (r guildRepository) FindById(id uint) (Guild, error) {
	var entity Guild
	err := r.DB.First(&entity, id).Error
	return entity, err
}

func (r guildRepository) Save(entity Guild) (Guild, error) {
	err := r.DB.Create(&entity).Error
	return entity, err
}

func (r guildRepository) Update(entity Guild) error {
	return r.DB.UpdateColumns(&entity).Error
}

func (r guildRepository) Delete(entity Guild) error {
	return r.DB.Delete(&entity).Error
}

func (r guildRepository) Count() (uint, error) {
	var count uint
	err := r.DB.Model(&Guild{}).Count(&count).Error
	return count, err
}
