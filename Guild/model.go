package Guild

import (
	"github.com/jinzhu/gorm"
)

type Guild struct {
	gorm.Model
	Name string `gorm:"size:80"`
}
